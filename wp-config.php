<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_atr_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';{A)L8*6]j-4F6nAG(R^FJX]G_c;NNZR=]EG.iS1=ZB[ML=Y;4br)bdR<B@Q@p@5' );
define( 'SECURE_AUTH_KEY',  ':a39e[5hr@g0jOF]AE`TUMmX;ev-NMvS/3O(#?I3sq(8{TnF/SZD(T#B*m0A8,n-' );
define( 'LOGGED_IN_KEY',    '~71b(6YBI%`cI#+le/ry]LLUCR{CilLmE-u/(4P}e.XZ2>@TWp5%G@TPq)#e#a8g' );
define( 'NONCE_KEY',        '(r:J?#XDd+^|^|5h7Nd?-z>xmX/dM5mNGw@xUm@fYQ4wJ`[^2d+/vPm(F<;o*i]u' );
define( 'AUTH_SALT',        'GFMI Wn~x(]TeG4?GCk!TPEY, j#6I(i(;-)<fE:&|wkc~s@=N1+%qd}TbI5Q<g_' );
define( 'SECURE_AUTH_SALT', 'K%TJ^/pzWR>E>$S# @|84vsxIy9~EHFg{,lDhiCST;]>s$-a/a{A]f{/Mi<VdN<V' );
define( 'LOGGED_IN_SALT',   'j$bJw,t#?VoO ?k;rBRJJGIo^0&i]i7k3,)s%9e9:yk-L/6n=1?r&q*!n<+C:@[l' );
define( 'NONCE_SALT',       'RC$7$?U]H# uVZ2=iPX5=g3EoLJhRrTaRUZ-u^VUlnf%f2xseG?WJ,9[BsF=%`VR' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'atr_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
