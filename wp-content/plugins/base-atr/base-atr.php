<?php

/**
 * Plugin Name: Base Atr
 * Description: Controle base do tema Atr.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */



    function baseAtr () {

		// TIPOS DE CONTEÚDO
		conteudosAtr();

		taxonomiaAtr();

		metaboxesAtr();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
	function conteudosAtr(){

		//TIPOS DE CONTEÚDO
		tipoDestaque();

		tipoEmpreendimentos();

		tipoPlanta();

		tipoParceiros();

		tipoLinhadoTempo();
		
	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoPlanta() {

		$rotulosPlanta = array(
								'name'               => 'Plantas',
								'singular_name'      => 'Planta',
								'menu_name'          => 'Plantas',
								'name_admin_bar'     => 'Plantas',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo Planta',
								'new_item'           => 'Novo Planta',
								'edit_item'          => 'Editar Planta',
								'view_item'          => 'Ver Planta',
								'all_items'          => 'Todas as Plantas',
								'search_items'       => 'Buscar Planta',
								'parent_item_colon'  => 'Das Plantas',
								'not_found'          => 'Nenhuma Planta cadastrado.',
								'not_found_in_trash' => 'Nenhuma Planta na lixeira.'
							);

		$argsPlanta 	= array(
								'labels'             => $rotulosPlanta,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-media-interactive',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'plantas' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('planta', $argsPlanta);

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoEmpreendimentos() {

		$rotulosEmpreendimentos = array(
								'name'               => 'Empreendimentos',
								'singular_name'      => 'empreendimento',
								'menu_name'          => 'Empreendimentos',
								'name_admin_bar'     => 'Empreendimentos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo empreendimento',
								'new_item'           => 'Novo empreendimento',
								'edit_item'          => 'Editar empreendimento',
								'view_item'          => 'Ver empreendimento',
								'all_items'          => 'Todos os empreendimentos',
								'search_items'       => 'Buscar empreendimento',
								'parent_item_colon'  => 'Dos empreendimentos',
								'not_found'          => 'Nenhum empreendimento cadastrado.',
								'not_found_in_trash' => 'Nenhum empreendimento na lixeira.'
							);

		$argsEmpreendimentos 	= array(
								'labels'             => $rotulosEmpreendimentos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-building',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'empreendimentos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('empreendimentos', $argsEmpreendimentos);

	}

	// CUSTOM POST TYPE PARCEIROS
	function tipoParceiros() {

		$rotulosParceiros = array(
								'name'               => 'Parceiros',
								'singular_name'      => 'parceiro',
								'menu_name'          => 'Parceiros',
								'name_admin_bar'     => 'Parceiros',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo parceiro',
								'new_item'           => 'Novo parceiro',
								'edit_item'          => 'Editar parceiro',
								'view_item'          => 'Ver parceiro',
								'all_items'          => 'Todos os parceiros',
								'search_items'       => 'Buscar parceiro',
								'parent_item_colon'  => 'Dos parceiros',
								'not_found'          => 'Nenhum parceiro cadastrado.',
								'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
							);

		$argsParceiros 	= array(
								'labels'             => $rotulosParceiros,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-universal-access',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'parceiros' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiro', $argsParceiros);

	}

	// CUSTOM POST TYPE PARCEIROS
	function tipoLinhadoTempo() {

		$rotulosLinhaDoTempo = array(
								'name'               => 'Linhas do tempo',
								'singular_name'      => 'linha',
								'menu_name'          => 'Linhas do tempo',
								'name_admin_bar'     => 'Linhas do tempo',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova linha',
								'new_item'           => 'Nova linha',
								'edit_item'          => 'Editar linha',
								'view_item'          => 'Ver linha',
								'all_items'          => 'Todas as linhas',
								'search_items'       => 'Buscar linha',
								'parent_item_colon'  => 'Das linhas',
								'not_found'          => 'Nenhuma linha do tempo cadastrado.',
								'not_found_in_trash' => 'Nenhuma linha do tempo na lixeira.'
							);

		$argsLinhaDotempo 	= array(
								'labels'             => $rotulosLinhaDoTempo,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-chart-line',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'linha-do-tempo' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('linhadotempo', $argsLinhaDotempo);

	}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaAtr () {

			taxonomiaEmpreendimentos();
			taxonomiaPlantas();

		}

		function taxonomiaEmpreendimentos() {

			$rotulosCategoriaEmpreendimentos = array(

											'name'              => 'Categorias de empreendimentos',
											'singular_name'     => 'Categorias de empreendimentos',
											'search_items'      => 'Buscar categoria',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria',
											'update_item'       => 'Atualizar categoria',
											'add_new_item'      => 'Nova categoria',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias',
										);

			$argsCategoriaEmpreendimentos 	= array(

											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaEmpreendimentos,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categorias-empreendimentos' ),
										);

			register_taxonomy( 'categoriaempreendimentos', array( 'empreendimentos' ), $argsCategoriaEmpreendimentos);
		}

		function taxonomiaPlantas() {

			$rotulosCategoriaPlantas = array(

											'name'              => 'Categorias de Plantas',
											'singular_name'     => 'Categorias de Plantas',
											'search_items'      => 'Buscar categoria',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria',
											'update_item'       => 'Atualizar categoria',
											'add_new_item'      => 'Nova categoria',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias',
										);

			$argsCategoriaPlantas 	= array(

											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaPlantas,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'categorias-plantas' ),
										);

			register_taxonomy( 'categoriaplantas', array( 'planta' ), $argsCategoriaPlantas);
		}

	/****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesAtr(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'ATR_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link destaque: ',
						'id'    => "{$prefix}link_destaque",
						'desc'  => 'Link destaque',
						'type'  => 'text'
					),	
				),
			);

			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxEmpreendimentos',
				'title'			=> 'Detalhes do empreendimento',
				'pages' 		=> array( 'empreendimentos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Logo Empreendimento',
						'id'    => "{$prefix}empreendimento_logo",
						'desc'  => 'Logo',
						'type'  => 'single_image'
					),
					array(
						'name'  => 'Foto principal home Carrossel 1',
						'id'    => "{$prefix}empreendimento_foto_principal_1",
						'desc'  => 'Foto background Single',
						'type'  => 'single_image'
					),		
					array(
						'name'  => 'Foto principal Empreendimento home Carrossel 2',
						'id'    => "{$prefix}empreendimento_foto_principal",
						'desc'  => 'Foto background Single',
						'type'  => 'single_image'
					),	
					array(
						'name'  => 'Dormitórios',
						'id'    => "{$prefix}empreendimento_dormitorios",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => '2 a 3 Dormitórios',
					),
					array(
						'name'  => ' Área privativa',
						'id'    => "{$prefix}empreendimento_areaprivativa",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => '55m² até 120m²',
					),
					array(
						'name'  => ' Data de entrega',
						'id'    => "{$prefix}empreendimento_entrega",
						'desc'  => '',
						'type'  => 'text',
						'placeholder'  => 'Junho de 2022 (Entrega)',
					),
					// array(
					// 	'name'  => ' ID do vídeo youtube',
					// 	'id'    => "{$prefix}empreendimento_id_video",
					// 	'desc'  => '',
					// 	'type'  => 'text',
					// 	'placeholder'  => '5RNkQaDcRAc',
					// ),
				),
			);

			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxEmpreendimentos_preco',
				'title'			=> 'Detalhes do empreendimento área preço',
				'pages' 		=> array( 'empreendimentos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Preço Empreendimento',
						'id'    => "{$prefix}empreendimento_preco",
						'desc'  => 'Preço',
						'type'  => 'text',
						'placeholder'  => '389.000,00',
					),
					array(
						'name'  => 'Preço Empreendimento Benefícios',
						'id'    => "{$prefix}empreendimento_preco_beneficios",
						'desc'  => 'Descrição dos benefícios',
						'type'  => 'textarea',
					),

					array(
					    'id'      => 'empreendimento_preco_beneficios_lista',
					    'name'    => 'Lista de benefícios',
					    'type'    => 'fieldset_text',

					    // Options: array of key => Label for text boxes
					    // Note: key is used as key of array of values stored in the database
					    'options' => array(
					        'icone'    => 'Url - Ícone',
					        'texto'    => 'Texto',
					    ),

					    // Is field cloneable?
					    'clone' => true,
					),	

					array(
						'name'  	 => 'Plantas: ',
						'id'    	 => "{$prefix}empreendimento_plantas",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'planta',
						'field_type' => 'select',
						'multiple'        => true,
					),
					
				),
			);


		$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxLocalizacao',
				'title'			=> 'Detalhes do empreendimento localização',
				'pages' 		=> array( 'empreendimentos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Empreendimento Descrição localização',
						'id'    => "{$prefix}empreendimento_localizacao_descricao",
						'desc'  => 'Descrição da localização',
						'type'  => 'textarea'
					),	
					array(
						'name'  => 'Foto localização',
						'id'    => "{$prefix}empreendimento_localizacao_foto",
						'desc'  => 'Foto localização',
						'type'  => 'single_image'
					),	
					array(
						'name'  => 'Endereço localização',
						'id'    => "{$prefix}empreendimento_localizacao_endereco",
						'desc'  => 'Endereço localização',
						'type'  => 'text'
					),	

					array(
						'name'  => 'Endereço localização Iframe',
						'id'    => "{$prefix}empreendimento_localizacao_endereco_Iframe",
						'desc'  => 'Iframe Goole mapas',
						'type'  => 'textarea'
					),

					array(
						'name'  => 'Estabelicimentos próximos',
						'id'    => "{$prefix}empreendimento_localizacao_estabelicimentos",
						'desc'  => '',
						'type'  => 'text',
						'clone'  => true,
					),	
					
				),
			);

		$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxGaleria',
				'title'			=> 'Detalhes do empreendimento galeria',
				'pages' 		=> array( 'empreendimentos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Empreendimento Galeria de Fotos',
						'id'    => "{$prefix}empreendimento_galeria",
						'type'  => 'image_upload'
					),	

					array(
						'name'  => 'Empreendimento Galeria de Vídeos',
						'id'    => "{$prefix}empreendimento_galeria_videos",
						'type'  => 'text',
						'desc'  => 'url vídeo',
						'clone'  => true,
					),	
				),
			);

		// METABOX DE PLATNAS
		$metaboxes[] = array(

			'id'			=> 'detalhesMetaboxPlantas',
			'title'			=> 'Detalhes da planta',
			'pages' 		=> array( 'planta' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Título da planta: ',
					'id'    => "{$prefix}planta_titulo",
					'placeholder'  => '101 - Garden',
					'type'  => 'text'
				),
				array(
					'name'  => 'Metragem Privativa: ',
					'id'    => "{$prefix}planta_metragem_privativa",
					'placeholder'  => '128m²',
					'type'  => 'text'
				),
				array(
					'name'  => 'Vagas: ',
					'id'    => "{$prefix}planta_vagas",
					'placeholder'  => '1',
					'type'  => 'text'
				),
				array(
					'name'  => 'Imagem planta',
					'id'    => "{$prefix}planta_imagem",
					'desc'  => 'Foto planta',
					'type'  => 'single_image'
				),		
			),
		);

		// METABOX DE PLATNAS
		$metaboxes[] = array(

			'id'			=> 'detalhesMetaboxLinhadotempo',
			'title'			=> 'Detalhes da linha',
			'pages' 		=> array( 'linhadotempo' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'         => 'Ano: ',
					'id'           => "{$prefix}linhadotempo_ano",
					'placeholder'  => '2018',
					'type'         => 'text'
				),
				array(
					'name'  => 'Texto: ',
					'id'    => "{$prefix}linhadotempo_texto",
					'type'  => 'textarea'
				),
			),
		);

			
    	
    	return $metaboxes;
		}


	function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

    /****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAtr');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseAtr();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );

?>