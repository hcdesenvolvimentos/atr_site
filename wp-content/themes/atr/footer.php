<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package atr
 */

global $configuracao;
$empreendimentos = new WP_Query( array( 'post_type' => 'empreendimentos', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );

?>

	<footer>
		<div class="full-container">
			<div class="row">
				<div class="col-md-4">
					<figure class="logo">
						<img src="<?= $configuracao['header_logo']['url'] ?>" alt="<?php echo get_bloginfo(); ?>">
						<figcaption class="hidden"><?php echo get_bloginfo(); ?></figcaption>
					</figure>
					<a target="_blank" href="https://www.google.com.br/maps/place/<?= $configuracao['configuracoes_atr_info_contato_geral_contato_endereco'] ?>" class="endereco"><?= $configuracao['configuracoes_atr_info_contato_geral_contato_endereco'] ?></a>
				</div>
				<div class="col-md-2">
					<div class="menu-footer">
						<h4 class="titulo">ATR</h4>
						<nav>
							<a href="<?= home_url('/contato/') ?>">Fale conosco</a>
							<a href="<?= home_url('/contato/') ?>">Trabalhe Conosco</a>
							<a href="<?= home_url('/category/news/') ?>">Imprensa</a>
							<a href="<?= home_url('/a-atr/') ?>">A ATR</a>
						</nav>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu-footer">
						<h4 class="titulo">Imóveis</h4>
						<nav>
							<?php 	while ( $empreendimentos->have_posts() ) : $empreendimentos->the_post(); ?>
							<a href="<?php echo  get_permalink(); ?>"><?php echo  get_the_title(); ?></a>
							<?php  endwhile; wp_reset_query(); ?>
						</nav>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu-footer">
						<h4 class="titulo">Entre em Contato</h4>
						<p><?= $configuracao['configuracoes_atr_info_contato_geral_contato_Plantao'] ?></p>
						<a href="tel:<?= $configuracao['configuracoes_atr_info_contato_geral_contato_telefone'] ?>"><?= $configuracao['configuracoes_atr_info_contato_geral_contato_telefone_site'] ?></a>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu-footer">
						<h4 class="titulo">Entre em Contato</h4>
						<ul>

							<?php if($configuracao['configuracoes_atr_info_contato_geral_contato_instagram']): ?>
							<li>
								<a href="<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_instagram']; ?>">
									<img src="<?= get_template_directory_uri() ?>/img/iconfinder_Instagram_Solid_1435170 (1)@1,5x.svg" alt="Instagram">
								</a>
							</li>
							<?php endif; if($configuracao['configuracoes_atr_info_contato_geral_contato_face']): ?>
							<li>
								<a href="<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_face']; ?>">
									<img src="<?= get_template_directory_uri() ?>/img/fb.svg" alt="Facebook">
								</a>
							</li>
							<?php endif; if($configuracao['configuracoes_atr_info_contato_geral_contato_twitter']): ?>
							<li>
								<a href="<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_twitter']; ?>">
									<img src="<?= get_template_directory_uri() ?>/img/tt.svg" alt="Twitter">
								</a>
							</li>
							<?php endif; if($configuracao['configuracoes_atr_info_contato_geral_contato_pinterest']): ?>
							<li>
								<a href="<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_pinterest']; ?>">
									<img src="<?= get_template_directory_uri() ?>/img/pt.svg" alt="Pinterest">
								</a>
							</li>
							<?php endif; if($configuracao['configuracoes_atr_info_contato_geral_contato_youtube']): ?>
							<li>
								<a href="<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_youtube']; ?>">
									<img src="<?= get_template_directory_uri() ?>/img/iconfinder_youtube_317714@1,5x.svg" alt="Youtube">
								</a>
							</li>
							<?php endif; if($configuracao['configuracoes_atr_info_contato_geral_contato_linkedin']): ?>
							<li><a href="<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_linkedin']; ?>">
								<img src="<?= get_template_directory_uri() ?>/img/iconfinder_social_linked_in_710289@1,5x.svg" alt="Linkedin"></a>
							</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="selosAtr">
						<img src="<?= get_template_directory_uri() ?>/img/ISO-2001.svg" alt="ISO-2001">
						<img src="<?= get_template_directory_uri() ?>/img/pbqp-h.png" alt="pbqp">
					</div>
				</div>
				<div class="col-md-6">
					<a href="<?= home_url('/contato/') ?>" class="button-padrao">Oferecer um terreno</a>
				</div>
				
			</div>
			<div class="copyright">
				<p>ATR Incorporadore LTDA. Todos os direitos reservados 2019</p>
				<a href="#">Política de Privacidade</a>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>

</body>
</html>