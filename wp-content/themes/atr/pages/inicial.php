<?php

/**
 * Template Name: Inicial 
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package atr
 */
	$destaques       = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
	$empreendimentos = new WP_Query( array( 'post_type' => 'empreendimentos', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
	

	// LISTA AS CATEGORIAS
// $categoriasacesso = get_terms( $taxonomia, array(
// 	'orderby'    => 'count',
// 	'hide_empty' => 0,
// 	'parent'	 => 0
// ));
	get_header(); 
?>

<div class="pg pg-inicial">
	<section class="secao-destaque">
		<h4 class="hidden">SEÇÃO DESTAQUE</h4>
		
		<div class="carrossel-destaque owl-carousel owl-theme">
			<?php while ( $destaques->have_posts() ) : $destaques->the_post(); ?>
				<div class="destaque" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);">
					<div class="container">
						<div class="item">
							<article>

								<figure class="hidden">
									<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
								</figure>
								<h2 class="titulo"><?php echo get_the_title() ?></h2>
								<p><?php echo get_the_content() ?></p>
								
								<?php if(rwmb_meta('ATR_link_destaque')): ?>
									<a href="<?php echo rwmb_meta('ATR_link_destaque');?>" class="button-padrao button-saiba-mais">Saiba mais</a>
								<?php endif; ?>

							</article>
						</div>
					</div>
				</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<div class="destaque-menor">
			<div class="container">
				<div class="carrossel-destaque-menor owl-carousel owl-theme">
					<?php 
						while ( $empreendimentos->have_posts() ) : $empreendimentos->the_post(); 
						global $post;
						$categoriasEmpreendimentos = get_the_terms( $post->ID, 'categoriaempreendimentos' ); 
						foreach($categoriasEmpreendimentos as $categoriasEmpreendimentos):
							if($categoriasEmpreendimentos->name == "Lançamento" || $categoriasEmpreendimentos->name == "Em construção"):
					 ?>
					<div class="item">
						<a href="<?php echo get_permalink(); ?>">
							<div class="row">
								<div class="col-md-6">
									<figure>
										<img src="<?php echo rwmb_meta('ATR_empreendimento_foto_principal_1')['full_url'];?>" alt="<?php echo get_the_title() ?>">
										<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
									</figure>
								</div>
								<div class="col-md-6">
									<article>
										
										<span class="projeto-status"><?php echo $categoriasEmpreendimentos->name ?></span>
										
										<h2 class="titulo"><?php echo get_the_title() ?></h2>
										<p><?php echo rwmb_meta('ATR_empreendimento_localizacao_descricao'); ?></p>
										<h5 class="dormitorio"><?php echo rwmb_meta('ATR_empreendimento_dormitorios'); ?></h5>
										<h5 class="medida"><?php echo rwmb_meta('ATR_empreendimento_areaprivativa'); ?></h5>
										<span class="entrega"><?php echo rwmb_meta('ATR_empreendimento_entrega'); ?></span>
									</article>
								</div>
							</div>
						</a>
					</div>
					<?php endif;endforeach;endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</section>
	
	<section class="secao-empreendimentos">
		<h4 class="hidden">Já Entregues</h4>
		<div class="full-container">
			<h3 class="titulo">Já Entregues</h3>
			<a  class="button-ver-todos"></a>
			<div id="carrossel-empreendimentos-entregues" class="owl-carousel">
				<?php 
					$contadorEmpreendimentos = 0; 
					while ( $empreendimentos->have_posts() ) : $empreendimentos->the_post(); 
						
						$categoriasEmpreendimentos = get_the_terms( $post->ID, 'categoriaempreendimentos' ); 
						foreach($categoriasEmpreendimentos as $categoriasEmpreendimentos):
							if($categoriasEmpreendimentos->name == "Pronto para morar"):
				?>
				<div class="item">
					<figure>
						<img src="<?php echo rwmb_meta('ATR_empreendimento_foto_principal')['full_url'];?>" alt="<?php echo rwmb_meta('ATR_empreendimento_foto_principal')['full_url'];?>">
						<figcaption class="hidden"><?php echo rwmb_meta('ATR_empreendimento_foto_principal')['full_url'];?></figcaption>
					</figure> 
					<article>
						<h2 class="titulo"><?php echo get_the_title(); ?></h2>
						<p class="dormitorio"><?php echo rwmb_meta('ATR_empreendimento_dormitorios'); ?></p>
						<p class="medida"><?php echo rwmb_meta('ATR_empreendimento_areaprivativa'); ?></p>
						<a href="<?php echo get_permalink() ?>" class="button-ver-todos">Saiba mais</a>
					</article>
				</div>
				<?php endif; endforeach; $contadorEmpreendimentos++; endwhile; wp_reset_query(); ?>
			</div>
		</div>
	</section>
	
	<section class="secao-sobre-atr">
		<h4 class="hidden">SEÇÃO SOBRE A ATR</h4>
		<div class="full-container">
			<div class="row">
				<div class="col-md-6">
					<figure>
						<img src="<?php echo $configuracao['configuracoes_atr_info_foto']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_info_foto']['url'] ?>">
					</figure>
				</div>
				<div class="col-md-6">
					<article>
						<img src="<?php echo $configuracao['configuracoes_atr_info_logo']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_info_logo']['url'] ?>">
						<h3 class="titulo"><?php echo $configuracao['configuracoes_atr_info_titulo'] ?></h3>
						<p><?php echo $configuracao['configuracoes_atr_info_breve_descricao'] ?></p>
						<div class="descricao">
							<span><?php echo $configuracao['configuracoes_atr_info_descricao'] ?></span>
							<ul>
								<?php 
									$configuracoes_atr_info_lista_itens =  $configuracao['configuracoes_atr_info_lista_itens']; 

									foreach($configuracoes_atr_info_lista_itens as $configuracoes_atr_info_lista_itens){
										echo "<li>".$configuracoes_atr_info_lista_itens."</li>";
									}
								?>
							</ul>
						</div>
						<a href="<?php echo $configuracao['configuracoes_atr_info_link'] ?>" class="button-padrao button-ver-todos">Saiba mais</a>
					</article>
				</div>
			</div>
		</div>
	</section>
	<?php 
		//BANNER PRINCIPAL
		include (TEMPLATEPATH . '/inc/rodape-page.php');
		include (TEMPLATEPATH . '/inc/secao-contato.php');
		include (TEMPLATEPATH . '/inc/instagram.php');
		include (TEMPLATEPATH . '/inc/atendimento.php');
	?>
</div>

<?php get_footer();