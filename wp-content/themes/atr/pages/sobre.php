<?php

/**
 * Template Name: Sobre 
 * Description: Página Sobre
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package atr
 */
	
	$linhadotempo = new WP_Query( array( 'post_type' => 'linhadotempo', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
	$parceiros = new WP_Query( array( 'post_type' => 'parceiro', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
	get_header(); 
?>

<div class="pg pg-sobre">
	
	<section class="secao-destaque">
		<h4 class="hidden"><?php echo get_the_title() ;?></h4>
		
		<div class="destaque" style="background-image: url(<?php echo $configuracao['configuracoes_atr_sobre_infos_banner']['url'] ?>)">
			<figure>
				<img src="<?php echo $configuracao['configuracoes_atr_sobre_infos_banner']['url'] ?>" alt="<?php echo get_the_title() ;?>" class="hidden">
			</figure>
			<div class="container">
				<h2 class="titulo"><?php echo get_the_title() ;?></h2>
			</div>
		</div>
		<div class="destaque-menor">
			<div class="container">
				<div id="0" class="item">
					<div class="row">
						<div class="col-md-3">
							<ul>
								<li data-id="0-item" class="button-padrao">Missão</li>
								<li data-id="1-item">Visão</li>
								<li data-id="2-item">Valores</li>
							</ul>
						</div>

						<div class="col-md-5 item-hidden active-item 0-item">
							<article class="topico-conteudo">
								<span class="topico">Missão</span>
								<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_sobre_infos_missao_titulo'] ?></h2>
								<p><?php echo $configuracao['configuracoes_atr_sobre_infos_missao_texto'] ?></p>
							</article>
						</div>
						<div class="col-md-4 item-hidden active-item 0-item">
							<figure style="background:url(<?php echo $configuracao['configuracoes_atr_sobre_infos_missao_foto']['url'] ?>); ">
								<img src="<?php echo $configuracao['configuracoes_atr_sobre_infos_missao_foto']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_sobre_infos_missao_foto']['url'] ?>">
								<figcaption class="hidden"><?php echo $configuracao['configuracoes_atr_sobre_infos_missao_foto']['url'] ?></figcaption>
							</figure>
						</div>

						<div class="col-md-5 item-hidden 1-item">
							<article class="topico-conteudo">
								<span class="topico">Visão</span>
								<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_sobre_infos_visao_titulo'] ?></h2>
								<p><?php echo $configuracao['configuracoes_atr_sobre_infos_visao_texto'] ?></p>
							</article>
						</div>
						<div class="col-md-4 item-hidden 1-item">
							<figure style="background:url(<?php echo $configuracao['configuracoes_atr_sobre_infos_visao_foto']['url'] ?>); ">
								<img src="<?php echo $configuracao['configuracoes_atr_sobre_infos_visao_foto']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_sobre_infos_visao_foto']['url'] ?>">
								<figcaption class="hidden"><?php echo $configuracao['configuracoes_atr_sobre_infos_visao_foto']['url'] ?></figcaption>
							</figure>
						</div>

						<div class="col-md-2 item-hidden 2-item">
							<article class="topico-conteudo">
								<span class="topico">Valores</span>
								<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_sobre_infos_valores_titulo'] ?></h2>
								<p><?php echo $configuracao['configuracoes_atr_sobre_infos_valores_texto'] ?></p>
							</article>
						</div>
						<div class="col-md-7 item-hidden 2-item">
							<figure style="background:url(<?php echo $configuracao['configuracoes_atr_sobre_infos_valores_foto']['url'] ?>); ">
								<img src="<?php echo $configuracao['configuracoes_atr_sobre_infos_valores_foto']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_sobre_infos_valores_foto']['url'] ?>">
								<figcaption class="hidden"><?php echo $configuracao['configuracoes_atr_sobre_infos_valores_foto']['url'] ?></figcaption>
							</figure>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="secao-servicos" >
		<h4 class="hidden"><?php echo $configuracao['configuracoes_atr_sobre_infos_texto_titulo'] ?></h4>
		<div class="right-side-container">
			<article class="conteudo-secao" id="video">
				<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_sobre_infos_texto_titulo'] ?></h2>
				<p><?php echo $configuracao['configuracoes_atr_sobre_infos_texto_texto'] ?></p>
				<ul>

					<?php
						$listServicos = $configuracao['configuracoes_atr_sobre_infos_texto_list'];

						foreach($listServicos as $listServicos):
							$listServicos = $listServicos;

							$listServicos = explode("|", $listServicos);
					?>
					<li><img src="<?php echo  $listServicos[0]; ?>" alt="<?php echo  $listServicos[1]; ?>"><?php echo  $listServicos[1]; ?></li>
					<?php endforeach; ?>
				</ul>
				<div class="video" >
					<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $configuracao['configuracoes_atr_sobre_infos_texto_video']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</article>
		</div>
	</section>
	
	<section class="secao-timeline">
		<h4 class="hidden">SEÇÃO TIMELINE</h4>
		<div class="conteudo-timeline">
			<div class="right-side-container">
				<h2 class="titulo">Time line</h2>
			</div>
			<ul class="timeline">
				<?php 

					$i = 0;
					while ( $linhadotempo->have_posts() ) : $linhadotempo->the_post(); 
					
					if($i == 0):
				?>
				<li><p class="active-time"><?php echo rwmb_meta('ATR_linhadotempo_ano');?></p></li>
				<?php else: ?>
				<li><p><?php echo rwmb_meta('ATR_linhadotempo_ano');?></p></li>
				<?php endif; $i++;endwhile; wp_reset_query(); ?>
			</ul>
			<ul id="carrossel-timeline" class="owl-carousel owl-theme">

				<?php while ( $linhadotempo->have_posts() ) : $linhadotempo->the_post(); ?>
				<li>
					<figure>
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
						<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
					</figure>
					<article>
						<span class="data"><?php echo rwmb_meta('ATR_linhadotempo_ano');?></span>
						<h2 class="titulo"><?php echo get_the_title() ?></h2>
						<p><?php echo rwmb_meta('ATR_linhadotempo_texto');?></p>
					</article>
				</li>
				<?php endwhile; wp_reset_query(); ?>

			</ul>
		</div>
	</section>
	
	<?php 
		//BANNER PRINCIPAL
		include (TEMPLATEPATH . '/inc/rodape-page.php');
		include (TEMPLATEPATH . '/inc/secao-contato.php');
		include (TEMPLATEPATH . '/inc/instagram.php');
		include (TEMPLATEPATH . '/inc/atendimento.php');
	?>

</div>

<?php get_footer();