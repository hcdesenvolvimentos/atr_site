<?php

/**
 * Template Name: Contato 
 * Description: Página Contato
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package atr
 */

	get_header(); 
?>

<div class="pg pg-contato">
	<div class="container">
		<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_pagina_contato_titulo'] ?></h2>
		<div class="secao-contato">
			<div class="row">
				<div class="col-md-7">
					<article class="info-contato">
						<p><?php echo $configuracao['configuracoes_atr_pagina_contato_texto'] ?></p>
						<div class="mapa">

							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14399.26499530086!2d-49.2021317!3d-25.544497!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd7c403462410d401!2sATR%20Incorporadora%20Ltda!5e0!3m2!1spt-BR!2sbr!4v1576603945949!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						</div>
						<a href="malito:<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_email'] ?>" class="contato email"><?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_email'] ?></a>
						<a href="tel:<?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_telefone'] ?>" class="contato tel"><?php echo $configuracao['configuracoes_atr_info_contato_geral_contato_telefone'] ?></a>
						<a href="https://www.google.com.br/maps/place/" class="contato endereco"><?php echo $configuracao['configuracoes_atr_info_contato_geral_horario_atendimento'] ?></a>
					</article>
				</div>
				<div class="col-md-5">
					<div class="formulario-contato">
						<?php echo do_shortcode('[contact-form-7 id="5" title="Formulário de contato"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();