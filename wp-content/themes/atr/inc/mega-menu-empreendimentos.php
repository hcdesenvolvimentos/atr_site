<?php 
	$k -= 0;			
	foreach($listarEmpreendimentos as $listarEmpreendimentos):
		$listarEmpreendimento = $listarEmpreendimentos;

		//QUERY CUSTON POST TYPE/ RECUPERANDO SERVIÇOS
		$empreendimentos = new WP_Query(
			array(
				'post_type'     => 'empreendimentos',
				'ordeby'        => 'id',
				'order'         => 'asc',
				'post_per_page' => 1,
				'tax_query'     => array(
				array(
					'taxonomy' => 'categoriaempreendimentos',
					'field'    => 'slug',
					'terms'    => $listarEmpreendimento->slug,
					)
				)

			)
		);
		
?>
<ul id="<?php echo $listarEmpreendimento->term_id; ?>" class="menuEmpreendimentos<?php echo $k ?>">
<?php 
	$j = 0; 
	while ( $empreendimentos->have_posts() ) : $empreendimentos->the_post(); 
		if($j == 0): 
?>
	<li 
		class                ="active-imovel"
		data-imagem          ="<?php echo rwmb_meta('ATR_empreendimento_foto_principal_1')['full_url'];?>"
		data-cateogira       ="<?php echo $listarEmpreendimento->name; ?>"
		data-dormitorio      ="<?php echo rwmb_meta('ATR_empreendimento_dormitorios'); ?>"
		data-areia-privativa ="<?php echo rwmb_meta('ATR_empreendimento_areaprivativa'); ?>"
		data-link            ="<?php echo get_permalink(); ?>"
	><?php echo get_the_title(); ?></li>
	
	<?php else: ?>
	
	<li
		data-imagem          ="<?php echo rwmb_meta('ATR_empreendimento_foto_principal_1')['full_url'];?>"
		data-cateogira       ="<?php echo $listarEmpreendimento->name; ?>"
		data-dormitorio      ="<?php echo rwmb_meta('ATR_empreendimento_dormitorios'); ?>"
		data-areia-privativa ="<?php echo rwmb_meta('ATR_empreendimento_areaprivativa'); ?>"
		data-link            ="<?php echo get_permalink(); ?>"
	><?php echo get_the_title(); ?></li>
	
	<?php endif; $j++; endwhile; wp_reset_query(); ?>
</ul>
<?php $k++; endforeach;?>