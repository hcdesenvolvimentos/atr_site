<?php
// DEFINE A TAXONOMIA
$taxonomia = 'categoriaempreendimentos';

// LISTA AS CATEGORIAS
$categoriaempreendimentos = get_terms( $taxonomia, array(
	'orderby'    => 'count',
	'hide_empty' => 0,
	'parent'	 => 0
));

// LISTA AS CATEGORIAS
$listarEmpreendimentos = get_terms( $taxonomia, array(
	'orderby'    => 'count',
	'hide_empty' => 0,
	'parent'	 => 0
));


?>
<div class="submenu">
	<img src="<?= get_template_directory_uri(); ?>/img/arrow-left.svg" alt="<?= get_template_directory_uri(); ?>/img/arrow-left.svg" class="imgHidden">
	<div class="row">
		
		<div class="col-sm-6">
			
			<article>
				<h2 class="titulo">Imóveis</h2>
				<nav>
					<?php 
						$i = 0;
						foreach($categoriaempreendimentos as $categoriaempreendimentos){
							$categoriaempreendimento = $categoriaempreendimentos;
							if($categoriaempreendimento->slug != "100-vendido"){

								if($i == 0){
									echo "<a href='#' class='active-imovel-status' data-id='".$categoriaempreendimento->term_id."'>".$categoriaempreendimento->name."</a>";
								}else{
									echo "<a href='#' data-id='".$categoriaempreendimento->term_id."'>".$categoriaempreendimento->name."</a>";
								}
							}
							$i++;
						}
					?>
				</nav>

				<?php include (TEMPLATEPATH . '/inc/mega-menu-empreendimentos.php'); ?>
			</article>

		</div>

		<div class="col-sm-6">
			<?php 
					//QUERY CUSTON POST TYPE/ RECUPERANDO SERVIÇOS
					$empreendimento = new WP_Query(
						array(
							'post_type'     => 'empreendimentos',
							'ordeby'        => 'id',
							'order'         => 'asc',
							'posts_per_page' => 1,
						)
					);

					while ( $empreendimento->have_posts() ) : $empreendimento->the_post(); 
					
			?>
			<div class="imagem-projeto">
				<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
				<figure>
					<img id="templateImagemEmpreendimento" src="<?php echo rwmb_meta('ATR_empreendimento_foto_principal_1')['full_url'];?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden" id="templateFigureEmpreendimento"><?php echo get_the_title() ?></figcaption>
				</figure>
				<div class="projeto-conteudo">
					<span id="templateCategoriaEmpreendimento"></span>
					<h2 class="titulo" id="templateNomeEmpreendimento"><?php echo get_the_title() ?></h2>
					<p class="dormitorio" id="templateDormEmpreendimento"><?php echo rwmb_meta('ATR_empreendimento_dormitorios'); ?></p>
					<p class="medida" id="templateAreaPrivEmpreendimento"><?php echo rwmb_meta('ATR_empreendimento_areaprivativa'); ?></p>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>

	</div>
</div>