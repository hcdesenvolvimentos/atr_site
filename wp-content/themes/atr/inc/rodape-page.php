
<?php 
	$parceiros       = new WP_Query( array( 'post_type' => 'parceiro', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
	$loopPost        = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );
?>

<section class="secao-social" style="background-image: url(<?php echo $configuracao['configuracoes_atr_info_responsabilidades_background']['url'] ?>)">
	<div class="full-container">
		<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_info_responsabilidades_titulo'] ?></h2>
		<p><?php echo $configuracao['configuracoes_atr_info_responsabilidades_descricao'] ?></p>
		<img src="<?php echo $configuracao['configuracoes_atr_info_responsabilidades_logo']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_info_responsabilidades_logo']['url'] ?>">
		<a href="#video" class="assistir-video scrollTop">Assistir o vídeo</a>
	</div>
</section>

<section class="secao-parceiros">
	<div class="container">
		<article>
			<h3 class="titulo"><?php echo $configuracao['configuracoes_atr_info_parceiros_titulo'] ?></h3>
			<p><?php echo $configuracao['configuracoes_atr_info_parceiros_texto'] ?></p>
		</article>
		<ul class="carrossel-parceiros owl-carousel owl-theme">

			<?php while ( $parceiros->have_posts() ) : $parceiros->the_post();  ?>
				<li><img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>"></li>
			<?php endwhile; wp_reset_query(); ?>

		</ul>
	</div>
</section>

<section class="secao-blog">
	<h4 class="hidden">SEÇÃO BLOG</h4>
	<div class="full-container">
		<ul class="lista-posts">
			<?php while ( $loopPost->have_posts() ) : $loopPost->the_post(); ?>
			<li>
				<a href="<?php echo get_permalink() ?>" class="link-imagem">
					<figure>
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>">
						<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
					</figure>
				</a>
				<div class="post-content">
					<a href="<?php echo get_permalink() ?>" class="link-categoria">Imprensa</a>
					<a href="<?php echo get_permalink() ?>" class="link-titulo">
						<h2 class="titulo-post"><?php echo get_the_title() ?></h2>
					</a>
					<span class="data"><?php the_time('j F  Y'); ?></span>
				</div>
			</li>
			<?php endwhile; wp_reset_query(); ?>
		</ul>
	</div>
</section>

<!-- <section class="secao-contato">
	<h4 class="hidden"><?php echo $configuracao['configuracoes_atr_contato_titulo'] ?></h4>
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="info-contato">
					<h4 class="titulo"><?php echo $configuracao['configuracoes_atr_contato_titulo'] ?></h4>
					<h6><?php echo $configuracao['configuracoes_atr_contato_subTitulo'] ?></h6>
					<p><?php echo $configuracao['configuracoes_atr_contato_texto'] ?></p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="formulario-contato">
					<?php echo do_shortcode('[contact-form-7 id="157" title="Formulário de contato Fixo"]'); ?>
				</div>
			</div>
		</div>
	</div>
</section> -->

<!-- <div class="instagram">
	<div class="full-container">
		<div id="instagram"><?php //echo do_shortcode('[instagram-feed]'); ?></div>
	</div>
</div> -->

<!-- <div class="atendimento">
	<div class="full-container">
		<div class="row">
			<div class="col-sm-3">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_atendimento_telefone@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Ligue agora</span>
					<a href="tel:<?= $configuracao['configuracoes_atr_info_contato_geral_contato_telefone'] ?>"><?= $configuracao['configuracoes_atr_info_contato_geral_contato_telefone_site'] ?></a>
				</p>
			</div>
			<div class="col-sm-3 text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_atendimento_online@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Atendimento</span>
					<a href="#">Online</a>
				</p>
			</div>
			<div class="col-sm-3 text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_chat_email@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Atendimento</span>
					<a href="malito:<?= $configuracao['configuracoes_atr_info_contato_geral_contato_email'] ?>">Por E-mail</a>
				</p>
			</div>
			<div class="col-sm-3 text-right">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_chat_whatsapp@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Atendimento</span>
					<a href="https://api.whatsapp.com/send?phone=<?= $configuracao['configuracoes_atr_info_contato_geral_contato_Whatsapp'] ?>&text=Ol%C3%A1%2C%20ATR!">Whatsapp</a>
				</p>
			</div>
		</div>
	</div>
</div> -->