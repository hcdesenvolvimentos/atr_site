<div class="atendimento">
	<div class="full-container">
		<div class="row text-center">
			<div class="col-sm-3">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_atendimento_telefone@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Ligue agora</span>
					<a href="tel:<?= $configuracao['configuracoes_atr_info_contato_geral_contato_telefone'] ?>"><?= $configuracao['configuracoes_atr_info_contato_geral_contato_telefone_site'] ?></a>
				</p>
			</div>
			<div class="col-sm-3" id="antendimentoOnline">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_atendimento_online@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Atendimento</span>
					<a >Online</a>
				</p>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_chat_email@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Atendimento</span>
					<a href="malito:<?= $configuracao['configuracoes_atr_info_contato_geral_contato_email'] ?>">Por E-mail</a>
				</p>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon_chat_whatsapp@1,5x.svg" alt="Ícone atendimento">
				<p>
					<span>Atendimento</span>
					<a href="https://api.whatsapp.com/send?phone=<?= $configuracao['configuracoes_atr_info_contato_geral_contato_Whatsapp'] ?>&text=Ol%C3%A1%2C%20ATR!">Whatsapp</a>
				</p>
			</div>
		</div>
	</div>
</div>