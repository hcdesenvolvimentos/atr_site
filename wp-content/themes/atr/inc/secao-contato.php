<section class="secao-contato">
	<h4 class="hidden"><?php echo $configuracao['configuracoes_atr_contato_titulo'] ?></h4>
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="info-contato">
					<h4 class="titulo"><?php echo $configuracao['configuracoes_atr_contato_titulo'] ?></h4>
					<h6><?php echo $configuracao['configuracoes_atr_contato_subTitulo'] ?></h6>
					<p><?php echo $configuracao['configuracoes_atr_contato_texto'] ?></p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="formulario-contato">
					<?php echo do_shortcode('[contact-form-7 id="157" title="Formulário de contato Fixo"]'); ?>
				</div>
			</div>
		</div>
	</div>
</section>