<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package atr
 */

get_header();

?>

<div class="pg pg-projeto">
	<section class="secao-destaque">
		<h4 class="hidden">SEÇÃO DESTAQUE</h4>
		<div class="destaque">
			<div class="container">
				<article>
					<h2 class="titulo">Ed. Valparaíso</h2>
					<p>Conforto e segurança na melhor localização do Afonso Pena” 2 Quartos 47 a 87m² Rua Desembargador Ernani de Abreu, 392 São José dos Pinhais – Paraná</p>
					<a href="#" class="button-padrao email">Envie um e-mail agora</a>
					<a href="#" class="button-padrao online">Atendimento online</a>
				</article>
				<figure>
					<img src="../img/Group 10@3x.png" alt="Logo projeto">
					<figcaption class="hidden">Logo projeto</figcaption>
				</figure>
			</div>
		</div>
	</section>
	<div class="menu-projeto">
		<div class="full-container">
			<nav>
				<a href="#secao-preço" class="active-iten">Preço</a>
				<a href="#secao-plantas">Plantas</a>
				<a href="#secao-localizacao">Localização</a>
				<a href="#secao-galeria">Galeria de imagens</a>
				<a href="#secao-diferenciais">Diferenciais</a>
			</nav>
		</div>
	</div>
	<section class="secao-preço" id="secao-preço">
		<h4 class="hidden">SEÇÃO PREÇO PROJETO</h4>
		<div class="container">
			<article>
				<span>A partir de</span>
				<h2 class="titulo">R$ 389.000,00</h2>
				<p>Experimente os benefícios da tecnologia ao seu favor em um local privilegiado. Tudo isso você encontra no Ed. Valparaíso. O imóvel localizado no Afonso Pena possui uma estrutura totalmente inovadora com seu design urbano e tecnológico em meio a diversas opções de compras e lazer.</p>
				<a href="#" class="button-padrao email">Envia um e-mail agora</a>
				<a href="#" class="button-padrao online">Atendimento online</a>
			</article>
			<ul>
				<li>Personalização de plantas</li>
				<li>Entrada Parcelas em até 12x</li>
				<li>Opção de vaga Dupla</li>
				<li>Fechadura biométrica</li>
				<li>Aquecimento a Gás</li>
				<li>Piso aquecido</li>
				<li>CFTV</li>
				<li>Vagas para Bicicletas</li>
			</ul>
		</div>
	</section>
	<section class="secao-plantas" id="secao-plantas">
		<h4 class="hidden">SEÇÃO PLANTAS PROJETO</h4>
		<div class="full-container">
			<article class="detalhes-planta">
				<h2 class="titulo">Plantas</h2>
				<table>
					<tr class="row-titulo">
						<th>Apartamento</th>
						<th>Metragem privativa</th>
						<th>Vagas</th>
						<th></th>
					</tr>
					<tr class="tr-active">
						<th class="radius-left">Garden final 01</th>
						<th>128m²</th>
						<th>1</th>
						<th class="seta radius-right"></th>
					</tr>
					<tr>
						<th class="radius-left">Garden final 01</th>
						<th>128m²</th>
						<th>1</th>
						<th class="seta radius-right"></th>
					</tr>
					<tr>
						<th class="radius-left">Garden final 01</th>
						<th>128m²</th>
						<th>1</th>
						<th class="seta radius-right"></th>
					</tr>
					<tr>
						<th class="radius-left">Garden final 01</th>
						<th>128m²</th>
						<th>1</th>
						<th class="seta radius-right"></th>
					</tr>
					<tr>
						<th class="radius-left">Garden final 01</th>
						<th>128m²</th>
						<th>1</th>
						<th class="seta radius-right"></th>
					</tr>
				</table>
			</article>
			<figure class="planta">
				<img src="../img/planta.png" alt="Planta projeto">
				<figcaption class="hidden">Planta projeto</figcaption>
			</figure>
		</div>
	</section>
	<section class="secao-localizacao" id="secao-localizacao">
		<h4 class="hidden">SEÇÃO LOCALIZAÇÃO</h4>
		<div class="left-side-container">
			<div class="row">
				<div class="col-sm-6">
					<figure>
						<img src="../img/local.png" alt="Imagem localização">
					</figure>
				</div>
				<div class="col-sm-6">
					<article>
						<h2 class="titulo">Localização</h2>
						<p>O imóvel localizado no Afonso Pena entrega conforto e sofisticação com opções de lazer e compras. Uma região que desperta um futuro para quem busca seus sonhos.</p>
						<h4 class="endereco"><a href="#">Av. Visconde de Guarapuava, 4628 - Batel - Curitiba - PR</a></h4>
						<div class="mapa">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.862330510683!2d-49.28687736270585!3d-25.44286576781165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce3884965e013%3A0xeddd41464efd2160!2sAv.%20Visc.%20de%20Guarapuava%2C%204628%20-%20Batel%2C%20Curitiba%20-%20PR%2C%2080240-010!5e0!3m2!1spt-BR!2sbr!4v1575900834807!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						</div>
						<h3 class="titulo">Estabelecimentos próximos</h3>
						<ul>
							<li>Academias</li>
							<li>Bancos</li>
							<li>Bares</li>
							<li>Hospital</li>
							<li>Igreja</li>
							<li>Lojas</li>
							<li>Restaurantes</li>
							<li>Supermercados</li>
							<li>Lojas</li>
							<li>Padarias</li>
							<li>Escolas</li>
							<li>Farmácias</li>
						</ul>
					</article>
				</div>
			</div>
		</div>
	</section>
	<section class="secao-galeria" id="secao-galeria">
		<h4 class="hidden">SEÇÃO GALERIA PROJETO</h4>
		<div class="mid-container">
			<div class="row">
				<div class="col-sm-3">
					<h2 class="titulo">Galeria</h2>
				</div>
				<div class="col-sm-6">
					<nav>
						<a href="#" class="galeria-ativa">Galeria de imagens</a>
						<a href="#">Vídeos</a>
					</nav>
				</div>
				<div class="col-sm-3 text-right">
					<a href="#" class="button-padrao">Download das imagens</a>
				</div>
			</div>
		</div>
		<div id="carrossel-galeria" class="owl-carousel owl-theme">
			<figure>
				<img src="../img/galeria.png" alt="Imagem projeto">
			</figure>
			<figure>
				<img src="../img/galeria.png" alt="Imagem projeto">
			</figure>
		</div>
	</section>
	<section class="secao-diferenciais" id="secao-diferenciais">
		<h4 class="hidden">SEÇÃO DIFERENCIAIS</h4>
		<div class="right-side-container">
			<div class="row">
				<div class="col-sm-5">
					<article>
						<img src="../img/detalhe-logo.svg" alt="Detalhes ATR">
						<h2 class="titulo">Diferenciais <br>ATR</h2>
						<p>Muito além de um empreendimento, entregamos qualidade de vida para você, com foco em tecnologia e sofisticação a cada projeto <br>finalizado. Confira alguns dos nossos diferenciais:</p>
					</article>
				</div>
				<div class="col-sm-7 text-right">
					<video src="" controls></video>
				</div>
			</div>
		</div>
		<div class="container">
			<ul>
				<li class="diferencial-left">
					<img src="../img/Group 18@3x.png" alt="Ícone diferenciais">
					<h2 class="titulo">Segurança</h2>
					<p>Entregamos segurança em todos os empreendimentos ATR. Possuímos circuito fechado de televisão, fechaduras biométricas em todos os apartamentos, vídeo porteiro e aplicativo integrado exclusivo para a sua máxima segurança.</p>
				</li>
				<li class="diferencial-center">
					<img src="../img/key (4)@3x.png" alt="Ícone diferenciais">
					<h2 class="titulo">Condições Exclusivas</h2>
					<p>Com a ATR você tem total personalização no momento da negociação. Entre em contato conosco e conheça nossas condições exclusivas de pagamento.</p>
				</li>
				<li class="diferencial-right">
					<img src="../img/smart-home@3x.png" alt="Ícone diferenciais">
					<h2 class="titulo">Inovação a cada empreendimento</h2>
					<p>A ATR investe em cada projeto uma nova forma de entregar conforto para você. Atualmente, nossos projetos já incluem: piso aquecido, aquecimento a gás, preparação para instalação de ar condicionado e tomadas usb.</p>
				</li>
			</ul>
		</div>
	</section>
	<section class="secao-contato">
		<h4 class="hidden">SEÇÃO CONTATO</h4>
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="info-contato">
						<h4 class="titulo">Olá, vamos bater um papo!</h4>
						<h6>Entre em contato com a ATR</h6>
						<p>Ficou com alguma dúvida sobre algum assunto? Fale conosco através do telefone <a href="#">(41) 3215-1234</a> ou mande um Whats para <a href="#">(41) 9 9123-4567</a>. Caso prefira, envie um email para nós através do formulário ao lado, entraremos em contato o mais breve possível.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="formulario-contato">
						<form>
							<input type="text" placeholder="Nome">
							<input type="email" placeholder="E-mail">
							<textarea placeholder="Mensagem"></textarea>
							<div class="button-enviar button-padrao">
								<input type="submit" value="Enviar">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer();