<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package atr
 */
	global $post;
	$categoriasEmpreedimento = wp_get_object_terms($post->ID, 'categoriaempreendimentos', array('orderby' => 'term_id', 'order' => 'ASC') );
	foreach($categoriasEmpreedimento as $categoriasEmpreedimento){
		if($categoriasEmpreedimento->name == "100% Vendido"){
		$nome = $categoriasEmpreedimento->name;
		}else{
			$nome ="R$ ". rwmb_meta('ATR_empreendimento_preco');
		}
	}
	get_header();
	$plantas = new WP_Query( array( 'post_type' => 'planta', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
?>
<div class="pg pg-projeto">
	
	<section class="secao-destaque">
		<h4 class="hidden"><?php echo get_the_title() ?></h4>
		<div class="destaque" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>)">
		
			<div class="container">
				<article>
					<h2 class="titulo"><?php echo get_the_title() ?></h2>
					<p><?php echo get_the_content() ?></p> 
					<a href="<?= home_url('/contato/') ?>" class="button-padrao email">Enviar um e-mail agora</a>
					<a href="https://api.whatsapp.com/send?phone=<?= $configuracao['configuracoes_atr_info_contato_geral_contato_Whatsapp'] ?>&text=Ol%C3%A1%2C%20ATR!" class="button-padrao online">Atendimento online</a>
				</article>
				<figure>
					<img src="<?php echo rwmb_meta('ATR_empreendimento_logo')['full_url'];?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
				</figure>
			</div>
		</div>
	</section>

	<div class="menu-projeto">
		<div class="full-container">
			<nav>
				<a href="#secao-preco" class="scrollTop active-iten">Preço</a>
				<a href="#secao-plantas" class="scrollTop" >Plantas</a>
				<a href="#secao-localizacao" class="scrollTop" >Localização</a>
				<a href="#secao-galeria" class="scrollTop" >Galeria de imagens</a>
				<a href="#secao-diferenciais" class="scrollTop" >Diferenciais</a>
			</nav>
		</div>
	</div>
	
	<section class="secao-preco" id="secao-preco">
		<h4 class="hidden">SEÇÃO PREÇO PROJETO</h4>
		<div class="container">
			<article>
				<span>A partir de</span>
				<h2 class="titulo"><?php echo $nome;?></h2>
				<p><?php echo rwmb_meta('ATR_empreendimento_preco_beneficios');?></p>
				<a href="#" class="button-padrao email">Envia um e-mail agora</a>
				<a href="#" class="button-padrao online">Atendimento online</a>
			</article>
			<ul>
				<?php 
					while ( have_posts() ) : the_post();
						$empreendimento_preco_beneficios_lista = rwmb_meta('empreendimento_preco_beneficios_lista');
					endwhile;


					foreach($empreendimento_preco_beneficios_lista as $empreendimento_preco_beneficios_lista):
						echo "<li><img src='".$empreendimento_preco_beneficios_lista['icone']."' alt='".$empreendimento_preco_beneficios_lista['texto']."'>".$empreendimento_preco_beneficios_lista['texto']."</li>";
					endforeach;
				?>
			</ul>
		</div>
	</section>

	<section class="secao-plantas" id="secao-plantas">
		<h4 class="hidden">SEÇÃO PLANTAS PROJETO</h4>
		<div class="full-container">
			<article class="detalhes-planta">
				<h2 class="titulo">Plantas</h2>
				<table>
					<tr class="row-titulo">
						<th>Apartamento</th>
						<th>Metragem privativa</th>
						<th>Vagas</th>
						<th></th>
					</tr>

					<?php 
					 	$idProjetos = rwmb_meta('ATR_empreendimento_plantas');
						while ( $plantas->have_posts() ) : $plantas->the_post(); 
							global $post;
							$verificacao =  in_array($post->ID,$idProjetos);
								if($verificacao):
								
					?>
					<tr data-image="<?php echo rwmb_meta('ATR_planta_imagem')['full_url'];?>">
						<th class="radius-left"><?php echo rwmb_meta('ATR_planta_titulo'); ?></th>
						<th><?php echo rwmb_meta('ATR_planta_metragem_privativa'); ?></th>
						<th><?php echo rwmb_meta('ATR_planta_vagas');?></th>
						<th class="seta radius-right"></th>
					</tr>
					<?php endif; $cont++; endwhile; wp_reset_query(); ?>

				</table>
			</article>
			<figure class="planta">
				<img src="<?php echo get_template_directory_uri(); ?>/img/planta.png" alt="Planta projeto">
				<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
			</figure>
		</div>
	</section>

	<section class="secao-localizacao" id="secao-localizacao">
		<h4 class="hidden">SEÇÃO LOCALIZAÇÃO</h4>
		<div class="left-side-container">
			<div class="row">
				<div class="col-lg-6">
					<figure>
						<img src="<?php echo rwmb_meta('ATR_empreendimento_localizacao_foto')['full_url']; ?>" alt="<?php echo rwmb_meta('ATR_empreendimento_localizacao_foto')['full_url']; ?>">
					</figure>
				</div>
				<div class="col-lg-6">
					<article>
						<h2 class="titulo">Localização</h2>
						<p><?php echo rwmb_meta('ATR_empreendimento_localizacao_descricao'); ?></p>
						<h4 class="endereco"><a href="#"><?php echo rwmb_meta('ATR_empreendimento_localizacao_endereco'); ?></a></h4>
						<div class="mapa">
							<iframe src="<?php echo rwmb_meta('ATR_empreendimento_localizacao_endereco_Iframe'); ?>" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
						</div>
						<h3 class="titulo">Estabelecimentos próximos</h3>
						<ul>
							<?php 
								$empreendimento_localizacao_estabelicimentos = rwmb_meta('ATR_empreendimento_localizacao_estabelicimentos');
								foreach($empreendimento_localizacao_estabelicimentos as $empreendimento_localizacao_estabelicimentos ): ?>
								<li><?php echo  $empreendimento_localizacao_estabelicimentos; ?></li>
							<?php endforeach; ?>
						</ul>
					</article>
				</div>
			</div>
		</div>
	</section>
	<section class="secao-galeria" id="secao-galeria">
		<h4 class="hidden">SEÇÃO GALERIA PROJETO</h4>
		<div class="mid-container">
			<div class="row">
				<div class="col-md-3">
					<h2 class="titulo">Galeria</h2>
				</div>
				<div class="col-md-6">
					<nav>
						<a href="#" data-id="0" class="galeria-ativa">Galeria de imagens</a>
						<?php 
						$i = 0;
						$empreendimento_galeria_videos_link = rwmb_meta('ATR_empreendimento_galeria_videos');
						foreach($empreendimento_galeria_videos_link as $empreendimento_galeria_videos_link ): 
							if($i < 1):
						?>
						<a href="#" data-id="1" >Vídeos</a>
						<?php endif;$i++;endforeach;?>
					</nav>
				</div>
				<div class="col-md-3 text-right">
					<a href="#" class="button-padrao">Download das imagens</a>
				</div>
			</div>
		</div>
		<div id="0" class="carrossel-galeria carrossel-foto owl-carousel owl-theme carrossel-galeria-active">
			<?php 
				$empreendimento_galeria = rwmb_meta('ATR_empreendimento_galeria');
				foreach($empreendimento_galeria as $empreendimento_galeria ): ?>
			<figure>
				<img src="<?php echo $empreendimento_galeria['full_url'] ?>" alt="<?php echo $empreendimento_galeria['full_url'] ?>">
			</figure>
			<?php endforeach; ?>
		</div>
		<div id="1" class="carrossel-galeria carrossel-video owl-carousel owl-theme">
			<?php 
				$empreendimento_galeria_videos = rwmb_meta('ATR_empreendimento_galeria_videos');
				foreach($empreendimento_galeria_videos as $empreendimento_galeria_videos ): ?>
			<div class="video">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $empreendimento_galeria_videos; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<?php endforeach; ?>
		</div>
	</section>
	<section class="secao-diferenciais" id="secao-diferenciais">
		<h4 class="hidden">SEÇÃO DIFERENCIAIS</h4>
		<div class="right-side-container">
			<div class="row">
				<div class="col-md-6">
					<article>
						<img src="<?php echo $configuracao['configuracoes_atr_diferenciais_logo']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_diferenciais_logo']['url'] ?>">
						<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_diferenciais_titulo'] ?></h2>
						<p><?php echo $configuracao['configuracoes_atr_diferenciais_descricao'] ?></p>
					</article>
				</div>
				<div class="col-md-6 text-center">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $configuracao['configuracoes_atr_diferenciais_video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<div class="container">
			<ul>
				<li class="diferencial-left">
					<img src="<?php echo $configuracao['configuracoes_atr_diferenciais_list_icone_1']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_diferenciais_list_icone_1']['url'] ?>">
					<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_diferenciais_list_titulo_1'] ?></h2>
					<p><?php echo $configuracao['configuracoes_atr_diferenciais_list_texto_1'] ?></p>
				</li>
				<li class="diferencial-center">
					<img src="<?php echo $configuracao['configuracoes_atr_diferenciais_list_icone_2']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_diferenciais_list_icone_2']['url'] ?>">
					<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_diferenciais_list_titulo_2'] ?></h2>
					<p><?php echo $configuracao['configuracoes_atr_diferenciais_list_texto_2'] ?></p>
				</li>
				<li class="diferencial-right">
					<img src="<?php echo $configuracao['configuracoes_atr_diferenciais_list_icone_3']['url'] ?>" alt="<?php echo $configuracao['configuracoes_atr_diferenciais_list_icone_3']['url'] ?>">
					<h2 class="titulo"><?php echo $configuracao['configuracoes_atr_diferenciais_list_titulo_3'] ?></h2>
					<p><?php echo $configuracao['configuracoes_atr_diferenciais_list_texto_3'] ?></p>
				</li>
			</ul>
		</div>
	</section>

	<?php 
		//BANNER PRINCIPAL
		include (TEMPLATEPATH . '/inc/secao-contato.php');
		include (TEMPLATEPATH . '/inc/atendimento.php');
	?>

</div>
<?php get_footer();