

	$(document).ready(function(){
		$('.carrossel-parceiros').owlCarousel({
			items : 5,
			dots: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: false,
			responsiveClass:true,			    
			responsive:{
				320:{
					items:1,

				},
				425:{
					items:3,

				},
				768:{
					items:4,

				},
				991:{
					items:5,

				},
			}
		});

		$('#carrossel-empreendimentos-entregues').owlCarousel({
			items : 3,
			dots: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: true,	
			smartSpeed: 450,
			center: false,
			responsiveClass:true,			    
			responsive:{
				320:{
					items:1,

				},
				425:{
					items:3,

				},
				
			}
		});
		//BOTÕES DO CARROSSEL DESTAQUE HOME
		var carrossel_empreendimentos = $("#carrossel-empreendimentos-entregues");
		carrossel_empreendimentos.owlCarousel();
		$('.pg-inicial .secao-empreendimentos .button-ver-todos').click(function(){ carrossel_empreendimentos.trigger('next.owl.carousel'); });


		$("#carrossel-timeline").owlCarousel({
			items: 2,
			dots: true,
			loop: false,
			margin: 24,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			autoWidth: false,
			center: true,
			nav: true,
		});

		$('.carrossel-galeria.carrossel-foto').owlCarousel({
			items: 2,
			dots: true,
			loop: true,
			margin: 60,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: true,
			responsiveClass:true,			    
			responsive:{
				320:{
					items: 1,
					center: false,
				},
				768:{
					items: 2,
					center: true,
				},
			}
		});
		$('.carrossel-galeria.carrossel-video').owlCarousel({
			items: 1,
			dots: true,
			loop: false,
			margin: 60,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: true,
			responsiveClass:true,			    
			responsive:{
				320:{
					items: 1,
					center: false,
				},
				768:{
					items: 2,
					center: true,
				},
			}
		});

		$('.carrossel-destaque-menor').owlCarousel({
			items: 1,
			dots: true,
			nav: false,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: true,	
			smartSpeed: 450,
			animateOut: 'fadeOut',
		});

		$('.carrossel-destaque').owlCarousel({
			items: 1,
			dots: true,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: true,	
			smartSpeed: 450,
			singleItem : true,
			animateOut: 'fadeOut',
		});

		$('.pg-sobre .secao-destaque .destaque-menor .col-md-3 ul li').click(function(e){
			$(".pg-sobre .secao-destaque .destaque-menor .col-md-3 ul li").removeClass('button-padrao');
			$(this).addClass('button-padrao');
			let dataId = $(this).attr('data-id');
			$('.pg-sobre .secao-destaque .item-hidden').removeClass('active-item');
			$('.pg-sobre .secao-destaque .item-hidden.' +dataId).addClass('active-item');
		});
	
	$('.pg-projeto .secao-plantas article table tr').click(function(){
		let dataImage = $(this).attr('data-image');

		$('.pg-projeto .secao-plantas article table tr').removeClass('tr-active');
		$(this).addClass('tr-active');

		$('.pg-projeto .secao-plantas .planta img').attr('src', dataImage);
	});

	$(".pg-projeto .secao-plantas article table tr:nth-child(2)").trigger( "click" );

	$('.pg-projeto .secao-galeria nav a').click(function(e){
		e.preventDefault();

		let dataId = $(this).attr('data-id');
		$('.pg-projeto .secao-galeria nav a').removeClass('galeria-ativa');
		$(this).addClass('galeria-ativa');

		$('.pg-projeto .secao-galeria .carrossel-galeria').removeClass('carrossel-galeria-active');
		$('.pg-projeto .secao-galeria #' + dataId + '.carrossel-galeria').addClass('carrossel-galeria-active');
	});

	$('a.scrollTop').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}

		}
	});

	$('header img.icone-menu').click(function(){
		$('header .menu-principal').addClass('menu-mobile');
		$('body').addClass('travaScroll');
	});


	$('header .imgHidden').click(function(){
		$('header .menu-principal ul li .submenu').slideUp();
	});

	$('header .menu-principal.menu-mobile ul li a.imoveis').click(function(){
		$('header .menu-principal ul li .submenu').slideDown();
	});

	$('header .menu-principal .icone-fechar-menu').click(function(){
		$('header .menu-principal').removeClass('menu-mobile');
		$('body').removeClass('travaScroll');
	});

	// setTimeout(function(){
	// 	$("header .menu-principal ul li .submenu article nav a:nth-child(1)").trigger( "click" );
	// 	$("header .menu-principal ul li .submenu article ul li:nth-child(1)").trigger( "click" );
	// }, 1000);
	
	

	$('header .menu-principal ul li .submenu article nav a').click(function(e){
		event.preventDefault();
		let dataID = $(this).attr('data-id');
		$('header .menu-principal ul li a').removeClass('active-imovel-status');
		$(this).addClass('active-imovel-status');
		alteraTemplateCateogiraMenu(dataID);
	});


	function alteraTemplateCateogiraMenu(dataID){
		$("header .menu-principal ul li .submenu article ul").fadeOut();
		$("header .menu-principal ul li .submenu article ul#"+dataID).fadeIn();
	}

	$('header .menu-principal ul li .submenu article ul li').click(function(e){
		$(this).addClass('active-imovel');
		let dataLink         = $(this).attr('data-link');
		window.location.href = dataLink;
	});

	$('header .menu-principal ul li .submenu article ul li').mouseover(function(e){
		// event.preventDefault();

		$('header .menu-principal ul li .submenu article ul li').removeClass('active-imovel');
		$(this).addClass('active-imovel');

		let dataImagem         = $(this).attr('data-imagem');
		let dataCateogira      = $(this).attr('data-cateogira');
		let dataDormitorio     = $(this).attr('data-dormitorio');
		let dataAreiaPrivativa = $(this).attr('data-areia-privativa');
		let dataNome           = $(this).text();
		
		$("header .menu-principal ul li .submenu .imagem-projeto ").addClass('ativeLoad');
		setTimeout(function(){ 
			$("header .menu-principal ul li .submenu .imagem-projeto ").removeClass('ativeLoad');
		}, 2500);

		setTimeout(function(){ 
			alteraTemplateEmpreemdimentoMenu(dataImagem,dataCateogira,dataDormitorio,dataAreiaPrivativa,dataNome);
		}, 100)
	});

	function alteraTemplateEmpreemdimentoMenu(dataImagem,dataCateogira,dataDormitorio,dataAreiaPrivativa,dataNome){
		$("#templateImagemEmpreendimento").attr('src', dataImagem);
		$("#templateImagemEmpreendimento").attr('alt', dataImagem);
		$("#templateFigureEmpreendimento").text(dataNome);
		$("#templateCategoriaEmpreendimento").text(dataCateogira);
		$("#templateNomeEmpreendimento").text(dataNome);
		$("#templateDormEmpreendimento").text(dataDormitorio);
		$("#templateAreaPrivEmpreendimento").text(dataAreiaPrivativa);
	
	}




	// var userFeed = new Instafeed({
 //       get: 'user',
 //       userId: '11635157636',
 //       clientId: '3fbc1cde73aa4fa49f5c4e9e74f97803',
 //       accessToken: '11635157636.1677ed0.702d4869149b423f889d49359ebe4e6a',
 //       resolution: 'standard_resolution',
 //       template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
 //       sortBy: 'most-recent',
 //       limit: 6,
 //       links: false
 //     });
 //     userFeed.run();

 	$('a.scrollTop').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
		
	});



	var listTimeline = document.querySelectorAll(".pg-sobre .secao-timeline .conteudo-timeline .timeline li");
	var arrayTimelineAno = [];
	
	for(var j = 0; j< listTimeline.length; j++){
		arrayTimelineAno.push(listTimeline[j].querySelector("p").textContent);
	}

	var listTimelinebtnCarrossel = document.querySelectorAll("#carrossel-timeline .owl-dots button");
	for(var i = 0; i< listTimelinebtnCarrossel.length; i++){
		listTimelinebtnCarrossel[i].querySelector("span").innerHTML=arrayTimelineAno[i]
	}



   $zopim(function() {
  	$zopim.livechat.hideAll();
  });

   $('#antendimentoOnline').click(function() {
		$zopim(function() {
		    $zopim.livechat.button.hide();
		  });
	});

});