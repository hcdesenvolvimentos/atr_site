<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package atr
 */
global $configuracao;
?>

<!DOCTYPE html>
<html lang="pt-br">
<head <?php language_attributes(); ?>>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?= get_template_directory_uri() ?>/img/favicon.ico" /> 

	<?php wp_head(); ?>

	<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?2dMJYYWuUMY5AtIv7zQwtxcudEkCe8pK";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
</head>

<body <?php body_class(); ?>>
	
	<header>
		<img class="icone-menu" src="<?= get_template_directory_uri() ?>/img/menu.svg" alt="Icone menu">
		<div class="row">
			<div class="col-sm-3">
				<a href="<?= home_url('/') ?>" class="logo">
					<figure>
						<img src="<?= $configuracao['header_logo']['url'] ?>" alt="<?php echo get_bloginfo(); ?>">
						<figcaption class="hidden"><?php echo get_bloginfo(); ?></figcaption>
					</figure>
				</a>
			</div>
			<div class="col-sm-6">
				<div class="menu-principal">
					<span class="icone-fechar-menu">X</span>
					<ul>
						<li><a href="<?= home_url('/a-atr/') ?>">a atr</a></li>
						<li>
							<a href="#" class="imoveis">imóveis</a>
							<?php 
								//BANNER PRINCIPAL
								include (TEMPLATEPATH . '/inc/mega-menu.php');
							?>
						</li>
						<li><a href="<?= home_url('/blog/') ?>">blog</a></li>
						<li><a href="<?= home_url('/contato/') ?>">contato</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-3 text-right">
				<a href="https://galeria.fabricadeaplicativos.com.br/app_atr" target="_blank" class="login button-padrao">login</a>
			</div>
		</div>
	</header>

	<a href="https://api.whatsapp.com/send?phone=<?= $configuracao['configuracoes_atr_info_contato_geral_contato_Whatsapp'] ?>&text=Ol%C3%A1%2C%20ATR!" class="whatsapp-mobile">
		<img src="<?php echo get_template_directory_uri() . '/img/icon_chat_whatsapp@1,5x.svg'; ?>" alt="Whatsapp">
	</a>